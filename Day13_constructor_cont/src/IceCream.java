
public class IceCream {
	String flavour;
	char size;
	boolean extraNuts;

	// 3 param custom constructor
	IceCream(String flavour, char size, boolean extraNuts) {
		this.flavour = flavour;
		this.size = size;
		this.extraNuts = extraNuts;
	}

	// 2 param custom constructor
	IceCream(String flavour, char size) {
		this.flavour = flavour;
		this.size = size;
	}

	// 1 param custom constructor
	IceCream(String flavour) {
		this.flavour = flavour;
	}

	IceCream() {

	}

	@Override
	public String toString() {
		return "IceCream [flavour=" + flavour + ", size=" + this.size + ", extraNuts=" + this.extraNuts + "]";
	}

	public static void main(String[] args) {
		IceCream i1 = new IceCream("Black current", 'L', true);

		IceCream i2 = new IceCream("Strawberry", 'M');

		IceCream i3 = new IceCream("Black current", 'L', false);

		IceCream i4 = new IceCream("Chocolate");
	}

}
