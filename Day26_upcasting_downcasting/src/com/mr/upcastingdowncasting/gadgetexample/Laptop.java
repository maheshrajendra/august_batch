package com.mr.upcastingdowncasting.gadgetexample;

public class Laptop extends Gadget {
	void playGame() {
		System.out.println("Playing game...");
	}
}
