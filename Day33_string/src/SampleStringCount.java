import java.util.Scanner;

// count the no. of words in the given sentence
public class SampleStringCount {
	public static void main(String[] args) {
		System.out.println("Enter a sentence");
		Scanner s = new Scanner(System.in);
		String sentence = s.nextLine();
		String[] words = sentence.split(" ");
		System.out.println("There are " + words.length + " in the given sentence");
	}
}
