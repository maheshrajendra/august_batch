
public class SampleWhileLoop {
	public static void main(String[] args) {
		int i = 198;
		while (i <= 200) {
			System.out.println(i++);
		}
		System.out.println("End of the program");
	}
}
