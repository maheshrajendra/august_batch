
public class Student {
	String name;
	int age;
	static String collegeName = "OXFORD";
		
	public static void main(String[] args) {
		System.out.println(Student.collegeName); // OXFORD
		Student s1 = new Student();
		System.out.println(s1.name); // null
		System.out.println(s1.age); // 0

		Student s2 = new Student();
		System.out.println(s2.name); // null
		System.out.println(s2.age); // 0

		System.out.println(s1.collegeName); // OXFORD
		System.out.println(s2.collegeName); // OXFORD

		s1.name = "Alpha";
		System.out.println(s1.name); // Alpha
		System.out.println(s2.name); // null
		
		Student.collegeName = "XYZIT";
		System.out.println(s1.collegeName); // XYZIT
		System.out.println(s2.collegeName); // XYZIT
		
	}
}
