
public class Student {
	String name;
	int age;
	long phoneNo;

	// ALT + SHIFT + S + O(OWE) -> constructor using fields
	public Student(String name, int age, long phoneNo) {
		this.name = name;
		this.age = age;
		this.phoneNo = phoneNo;
	}

	// override the toString() -> alt + shift + s + s
	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", phoneNo=" + phoneNo + "]";
	}

	public static void main(String[] args) {
		Student s1 = new Student("Alpha", 25, 937286387L);
		Student s2 = new Student("Beta", 23, 846574909L);

		Student[] students = { s1, s2 };
		System.out.println(s1);
		System.out.println(s2);

		System.out.println("______________");
		// we are print the elements present in Student array,
		// it usually prints the address of the objects stored,
		// but we have overridden the toString()
		// so it prints the details
		System.out.println(students[0]);
		System.out.println(students[1]);
	}

}
