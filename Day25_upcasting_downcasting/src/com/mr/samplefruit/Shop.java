package com.mr.samplefruit;

public class Shop {

	// we are writing Fruit as return type, because the method is returning more
	// than one object
	Fruit sell(int option) {
		if (option == 1) {
			return new Apple();
		} else {
			return new Mango();
		}
	}

	// you are main method
	public static void main(String[] args) {
		Shop s = new Shop();
		Fruit f = s.sell(1); // at this line, upcasting happens like ex: Fruit f = new Apple();
		f.clean();
		// we are downcasting to apple type, to access the sub class specific methods
		// IMP: to access sub class specific methods we must downcast
		if (f instanceof Apple) {
			Apple a = (Apple) f;
			// we are calling the class specific method of apple class
			a.prepareMilkShake();
		} else if (f instanceof Mango) {
			Mango m = (Mango) f;
			m.prepareJuice();
		}
	}
}
