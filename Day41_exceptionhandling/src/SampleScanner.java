import java.util.Scanner;

public class SampleScanner {
	public static void main(String[] args) {
		try {
			Scanner s = new Scanner(System.in);
			System.out.println("Enter a number");
			int x = s.nextInt();
			System.out.println(x);
		} catch (Exception e) {
			System.out.println("Some problem occured");
		}

		System.out.println("End of the program");
	}
}
