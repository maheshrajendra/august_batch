import java.util.Arrays;
import java.util.LinkedList;

public class SampleLinkedList {
	public static void main(String[] args) {
		LinkedList<Integer> list = new LinkedList<>(Arrays.asList(10, 20, 30, 20, 10, 20, null, null, 30));

		System.out.println(list);
	}
}
