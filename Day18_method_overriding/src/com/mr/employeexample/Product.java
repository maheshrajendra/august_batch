package com.mr.employeexample;

public class Product {
	String name;
	double price;
	int qty;
	boolean isInStock;

	public Product(String name, double price, int qty, boolean isInStock) {
		this.name = name;
		this.price = price;
		this.qty = qty;
		this.isInStock = isInStock;
	}

	@Override
	public String toString() {
		return "(name->" + name + "_ price->" + price + "_ qty->" + qty + "_ isInStock->" + isInStock + ")";
	}

	public static void main(String[] args) {
		Product p = new Product("Laptop", 56000, 3, true);
		System.out.println(p.toString());
		System.out.println(p);
	}
}
