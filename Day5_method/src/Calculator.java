
public class Calculator {

	// method advantage : re-usability
	// non-static method/instance method -> we must
	// create object and call with help of object reference and [.] operator
	void add(int x, int y) {
		System.out.println(x + y);
	}

	// static method can be called directly
	static void multiply(int x, int y) {
		System.out.println(x * y);
	}

	void div(int x, int y) {
		System.out.println(x / y);
	}

	static void sub(int x, int y) {
		System.out.println(x - y);
	}

	public static void main(String[] args) {
		Calculator c = new Calculator();
		c.add(10, 30);

		c.add(56878, 2385);

		c.add(789, 678);

		multiply(10, 20);
		multiply(100,300);
		multiply(45,5);

		c.div(45, 5);

		sub(20, 30);
	}

}
