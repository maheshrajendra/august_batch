
public class SimCard {
	long id;
	String brand;
	long simNo;

	SimCard(long id, String brand, long simNo) {
		this.id = id;
		this.brand = brand;
		this.simNo = simNo;
	}

	SimCard(long id, long simNo) {
		this.id = id;
		this.simNo = simNo;
	}

	@Override
	public String toString() {
		return "SimCard [id=" + id + ", brand=" + brand + ", simNo=" + simNo + "]";
	}

	public static void main(String[] args) {
		SimCard s1 = new SimCard(93646386560L, "Airtel", 9836365362L);
		SimCard s2 = new SimCard(98723987239L, "JIO", 8976547897L);

		SimCard s3 = new SimCard(92739464387L, 9383736368L);

		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);

		s3.brand = "VI";

		System.out.println(s3);
	}
}
