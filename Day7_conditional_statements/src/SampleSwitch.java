
public class SampleSwitch {
	public static void main(String[] args) {
		int num = 10;
		switch (num) {
		case 3:
			System.out.println("Three");
			break;
		case 2:
			System.out.println("Two");
			break;
		case 1:
			System.out.println("one");
			break;
		default:
			System.out.println("Some other number");
			break;
		}
	}
}
