
public class Employee {
	int id;
	String name;
	long phoneNo;

	// 3 param custom constructor -> ALT + SHIFT + S + O (OWE)
	public Employee(int id, String name, long phoneNo) {
		this.id = id;
		this.name = name;
		this.phoneNo = phoneNo;
	}

	// ALT + SHIFT + S + S
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", phoneNo=" + phoneNo + "]";
	}

	public static void main(String[] args) {
		Employee e = new Employee(1, "Mark", 97326482534L);
		System.out.println(e);

		Employee e2 = new Employee(2, "Charlie", 89632287362L);
		System.out.println(e2);

		Employee e3 = new Employee(3, "Steve", 85384528346L);
		System.out.println(e3);

		// this will not work as we do not have zero param constructor
		// Employee e4 = new Employee();
	}
}
