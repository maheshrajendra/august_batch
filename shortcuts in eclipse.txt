Shortcut keys in eclipse

basic:

ctrl + c -> copy 
ctrl + x -> cut
ctrl + v -> paste
ctrl + z -> undo
ctrl + y -> redo
ctrl = -> zoom in 
ctrl - -> zoom out


ctrl + d -> delete one line of code
ALT + upper arrow -> will move one line of code upwards
ALT + lower arrow -> will move one line of code downwards
ctrl + O -> will show the outline(shows the variables and methods present in the class)

ALT + SHIFT + S + O -> generate constructor using fields
ALT + SHIFT + S + S -> generate toString()
ALT + SHIFT + S + R -> generate getters and setters

main ctrl + space -> main method declaration
sysout ctrl +  space -> print statement
ctrl + F -> format the code (Arranges the code properly)
ctrl + A, ctrl + I ( indentation, i.e., arranging the code)
ctrl + s -> save the current file
ctrl + Shift + s -> save all the opened files.
ALT + ->(right arrow) will go to the previously opened file
ALT + <-(left arrow) will go to the next file
ctrl + shift + o  -> organize the imports
ALT + SHIFT + Q, P -> package explorer
ALT + SHIFT + Q, c -> console
ctrl + f11 -> to execute the program
first select the member to rename and then press ALT + SHIFt + R -> refactor and rename

ctrl + 1, enter -> will give the left hand side assignment
ctrl and click -> will traverse to the class/method/constructor
ctrl + N -> For any new resource

ctrl + shift + L -> to see all the available shortcuts
Ctrl + shift + x -> to upper case
ctrl + shift + y -> to lower case 

ctrl + / -> toggling single line comment 
ctrl + shift + / -> multiline comment


loops: 
1. forea| ctrl + space -> for each loop
2. for| ctrl+space -> for loop ( index over an array)
3. switch| ctrl+space -> switch case statement
4. while| ctrl+space -> while loop






