import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

public class Airline {
	int id;
	String name;
	double cost;
	int seatingCapacity;

	public Airline(int id, String name, double cost, int seatingCapacity) {
		this.id = id;
		this.name = name;
		this.cost = cost;
		this.seatingCapacity = seatingCapacity;
	}

	@Override
	public String toString() {
		return "Airline [id=" + id + ", name=" + name + ", cost=" + cost + ", seatingCapacity=" + seatingCapacity
				+ "]\n";
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Airline other = (Airline) obj;
		return id == other.id;
	}

	public static void main(String[] args) {
		Airline a1 = new Airline(1233, "Ethihad", 35000, 200);
		Airline a2 = new Airline(1897, "Air India", 25000, 170);
		Airline a3 = new Airline(9657, "Jet Airways", 45000, 180);
		Airline a4 = new Airline(9234, "Lufthansa", 38000, 210);
		Airline a5 = new Airline(4563, "Air Arabia", 32000, 220);
		Airline a6 = new Airline(8345, "Indigo", 36000, 245);
		Airline a7 = new Airline(8345, "Indigo", 36000, 245);
		Airline a8 = new Airline(9234, "Lufthansa", 38000, 210);
		
		HashSet<Airline> flights = new HashSet<>(Arrays.asList(a1, a2, a3, a4, a5, a6, a7, a8));
		System.out.println(flights);
	}
}
