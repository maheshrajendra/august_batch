
public class Truck extends Vehicle {
	int loadCapacity;

	@Override
	public String toString() {
		return "Truck [color=" + color + ", price=" + price + ", brand=" + brand + ", loadCapacity=" + loadCapacity
				+ "]";
	}

	void dropLoad() {
		System.out.println("Drop load...");
	}
}
