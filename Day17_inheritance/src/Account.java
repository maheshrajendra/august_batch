
public class Account {
	String accType;
	String accHolderName;
	double balance;
	long custId;
	boolean isEligibleForLoan;

	public Account(long custId, String accHolderName, double balance) {
		this.custId = custId;
		this.accHolderName = accHolderName;
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Account [accType=" + accType + ", accHolderName=" + accHolderName + ", balance=" + balance + ", custId="
				+ custId + ", isEligibleForLoan=" + isEligibleForLoan + "]";
	}

}
