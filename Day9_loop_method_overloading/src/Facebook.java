
public class Facebook {

	void login(String email, String password) {
		System.out.println("Logged in using email address");
	}

	void login(long phoneNo, String password) {
		System.out.println("Logged in using phone number");
	}

	public static void main(String[] args) {
		Facebook f = new Facebook();
		f.login("Alpha@gmail.com", "Alpha@123");
		f.login(98732968326482L, "Alpha@123");
	}
}
