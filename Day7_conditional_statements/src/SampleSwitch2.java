
public class SampleSwitch2 {
	public static void main(String[] args) {
		String key = "yes";
		switch (key) {
		case "no":
		case "NO":
			System.out.println("User denied terms and conditions");
			break;
		case "yes":
		case "YES":
			System.out.println("User accepted terms and conditions");

		}
	}
}
