
public class Student {
	String name;
	int age;
	String collegeName = "Oxford";

	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + "]";
	}

	public static void main(String[] args) {
		Student s = new Student();
		System.out.println(s);
		s.name = "Alpha";
		s.age = 25;
		System.out.println("After initializing the object");
		System.out.println(s);
	}
}
