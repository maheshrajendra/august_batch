
public class AccountUtility {

	static void generateAccountType(Account a) {
		double balance = a.balance;
		if (balance >= 10000) {
			a.accType = "Current";
		} else if (balance >= 1000 && balance < 10000) {
			a.accType = "Savings";
		} else if (balance >= 500 && balance < 1000) {
			a.accType = "Student";
		} else {
			a.accType = "Dormant";
		}
	}

	static void blackListAccount(Account a) {
		if (a.balance >= 1000000) {
			a.accType = "Blacklisted";
		}
	}

	static void markLoanEligibility(Account a) {
		if (a.balance >= 500000) {
			a.isEligibleForLoan = true;
		}
	}

	public static void main(String[] args) {
		Account myAccount = new Account(9638563286L, "Alpha", 78000);
		System.out.println(myAccount);
		AccountUtility.generateAccountType(myAccount);
		AccountUtility.generateAccountType(myAccount);
		AccountUtility.markLoanEligibility(myAccount);
		System.out.println(myAccount);
	}

}
