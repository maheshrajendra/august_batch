public class SampleUpperCaseLogic {
	static void printInUpperCase(String input) {
		System.out.println(input.toUpperCase());
	}

	public static void main(String[] args) {
		System.out.println("Enter your input");
		SampleUpperCaseLogic.printInUpperCase("Alpha");
	}
}
