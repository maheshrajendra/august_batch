
public class FirefoxDriver implements WebDriver {

	@Override
	public void get(String url) {
		System.out.println("opening firefox browser with " + url);
	}

	@Override
	public void quit() {
		System.out.println("quitting the firefox browser");
	}

	@Override
	public void openPrivateWindow() {
		System.out.println("opening private window");
	}

}
