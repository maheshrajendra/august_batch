class MobilePhone {
	double price;
	String color;
	String brand;

	public static void main(String[] args) {
		MobilePhone m1 = new MobilePhone();
		System.out.println(m1); // address
		System.out.println(m1.price); // 0.0
		m1.price = 75000;
		m1.brand = "Apple";
		m1.color = "White";

		System.out.println(m1.price);
		System.out.println(m1.color);
		System.out.println(m1.brand);
		
		MobilePhone m2 = new MobilePhone();
		System.out.println(m2);
	}
}
