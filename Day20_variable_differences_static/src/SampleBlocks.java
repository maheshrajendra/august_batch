
public class SampleBlocks {

	{
		System.out.println("Instance block");
	}

	{
		System.out.println("Instance block 2");
	}

	static {
		System.out.println("Static block");
	}

	static {
		System.out.println("Static block 2");
	}

	public static void main(String[] args) {
		System.out.println("main method");
		SampleBlocks s = new SampleBlocks();
		SampleBlocks s2 = new SampleBlocks();
	}

}
