
public class ChromeDriver implements WebDriver {

	@Override
	public void get(String url) {
		System.out.println("opening chrome browser with " + url);
	}

	@Override
	public void quit() {
		System.out.println("quitting the chrome browser");
	}

	@Override
	public void openPrivateWindow() {
		System.out.println("opening incognito window");
	}

}
// first step create class with name ChromeDriver and under interfaces choose add button
// type WebDriver, it will show the interface select it and press finish.
// it will create ChromeDriver with required details