
public class Bike extends Vehicle {
	boolean hasKicker;

	void kick() {
		System.out.println("Kick start the bike...");
	}

	@Override
	public String toString() {
		return "Bike [color=" + color + ", price=" + price + ", brand=" + brand + ", hasKicker=" + hasKicker + "]";
	}

}
