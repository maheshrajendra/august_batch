
public class ArrayUtility {

	static void display(int[] arr) {
		for (int i : arr) {
			System.out.print(i + " ");
		}
	}

	static void display(String[] arr) {
		for (String s : arr) {
			System.out.print(s + " ");
		}
	}

	public static void main(String[] args) {
		int[] nums = { 67, 98, 87, 534, 34 };

		// When we pass an array to a method just pass the name of that array
		display(nums);

		System.out.println("________________");
		int values[] = { 19, 89, 30, 94, 25, 17 };
		display(values);

		String[] colors = { "red", "blue", "green" };
		display(colors);
	}

}
