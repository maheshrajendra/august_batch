
public class SampleArrayUsingPartialTraverse {
	public static void main(String[] args) {
		int[] nums = { 10, 20, 30, 40, 50, 60, 70 };

		System.out.println("Print first half in forward direction");
		for (int i = 0; i <= nums.length / 2; i++) {
			System.out.println(nums[i]);
		}

		System.out.println("Print last half in reverse direction");
		for (int i = nums.length - 1; i >= nums.length / 2; i--) {
			System.out.println(nums[i]);
		}
	}
}
