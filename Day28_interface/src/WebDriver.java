
// in interface you can write only 
// 1. public static final constants
// 2. public abstract methods
public interface WebDriver {

	public abstract void get(String url);

	void quit();

	void openPrivateWindow();
	
}
