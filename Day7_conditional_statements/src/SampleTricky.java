
public class SampleTricky {
	public static void main(String[] args) {
		int marks = 15;
		// it will execute all the if statements
		// as marks value is less than 60, 75 and 35
		if (marks <= 60) {
			System.out.println("second class");
		}
		if (marks <= 75) {
			System.out.println("first class");
		}
		if (marks <= 35) {
			System.out.println("Pass");
		}
	}
}
