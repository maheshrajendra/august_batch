
public class Sample {
	{
		System.out.println("Instance block");
	}

	static {
		System.out.println("Static block");
	}

	public static void main(String[] args) {
		System.out.println("main method");

		Sample s = new Sample();

		Sample s2 = new Sample();

		Sample s3 = new Sample();

	}
}
