import java.util.ArrayList;

public class SampleArrayListWithGenerics {
	public static void main(String[] args) {
		ArrayList<Integer> numbersList = new ArrayList<>();
		numbersList.add(10);
		numbersList.add(null);
		numbersList.add(20);
		numbersList.add(30);
		numbersList.add(40);
		numbersList.add(50);

		System.out.println("Total elements are " + numbersList.size()); // Total elements are 6

		System.out.println(numbersList); // [10, null, 20, 30, 40, 50]

		numbersList.clear();

		System.out.println(numbersList); // []
	}
}
