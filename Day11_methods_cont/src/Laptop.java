
public class Laptop {
	String brand;
	int price;

	@Override
	public String toString() {
		return "Laptop [brand=" + brand + ", price=" + price + "]";
	}

	public static void main(String[] args) {
		Laptop laptop = new Laptop();
		laptop.brand = "HP";
		laptop.price = 50000;
		System.out.println(laptop);

		Laptop laptop2 = new Laptop();
		laptop2.brand = "Lenovo";
		laptop2.price = 60000;
		System.out.println(laptop2);
	}

}
