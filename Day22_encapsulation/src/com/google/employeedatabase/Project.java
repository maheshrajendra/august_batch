package com.google.employeedatabase;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class Project {
	private int projectId;
	private String projectName;
	private String projectClientCompany;
	private double budget;
}
