import java.util.Scanner;

public class ExampleScannerPrintStatement {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter a statement");
		String statement = s.nextLine();
		System.out.println(statement);
	}
}
