import java.util.LinkedHashMap;

public class ExampleLambda {
	public static void main(String[] args) {
		LinkedHashMap<String, Long> phoneMap = new LinkedHashMap<>();
		phoneMap.put("Alpha", 9374597394L);
		phoneMap.put("Beta", 6573635871L);
		phoneMap.put("Charlie", 1425374599L);

		// print only keys using lambda and forEach method
		phoneMap.forEach((k, v) -> {
			System.out.print(k + " ");
		});

		System.out.println("\n_______________");
		// print only values
		phoneMap.forEach((k, v) -> {
			System.out.print(v + " ");
		});

		System.out.println("\n_______________");
		// print both key and values
		phoneMap.forEach((k, v) -> {
			System.out.println(k + " " + v);
		});

	}
}
