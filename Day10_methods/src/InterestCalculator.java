
public class InterestCalculator {

	static void simpleInterest(int p, int t, double r) {
		double si = (p * t * r) / 100;
		System.out.println("The interest is " + si + " Rupees");
	}

	public static void main(String[] args) {
		simpleInterest(100000, 1, 5.5);

		simpleInterest(250000, 1, 6.5);
	}
}
