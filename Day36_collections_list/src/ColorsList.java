import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ColorsList {
	public static void main(String[] args) {
		ArrayList<String> colors = new ArrayList<>(Arrays.asList("Red", "Green", "Blue", "Yellow"));

		for (String color : colors) {
			System.out.println(color);
		}

		System.out.println("\n\n\nPrint in alphabetical ascending order");
		Collections.sort(colors);

		System.out.println(colors);

		System.out.println("\n\n\nSort in descending order");
		Collections.sort(colors, Collections.reverseOrder());
		System.out.println(colors);
	}
}
