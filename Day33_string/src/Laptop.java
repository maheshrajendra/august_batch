
public class Laptop{
	String brand;
	int price;
	double screenSize;

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Laptop [brand=");
		builder.append(brand);
		builder.append(", price=");
		builder.append(price);
		builder.append(", screenSize=");
		builder.append(screenSize);
		builder.append("]");
		return builder.toString();
	}

}
