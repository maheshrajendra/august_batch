
public class A {

	{
		System.out.println("instance block");
	}

	A() {
		super();
		System.out.println("Constructor");
	}

	public static void main(String[] args) {
		A a = new A();
	}

}
