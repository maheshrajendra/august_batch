
public class ReverseString {
	public static void main(String[] args) {
		String x = "hello";
		StringBuilder rev = new StringBuilder();
		for (int i = x.length() - 1; i >= 0; i--) {
			rev = rev.append(x.charAt(i));
		}

		System.out.println("Actual String " + x); // hello
		System.out.println("Reverse String " + rev); // olleh
	}
}
