
public class SamplePromotion {
	static void check(int x) {
		System.out.println("int method " + x);
	}

	static void check(byte x) {
		System.out.println("byte method " + x);
	}

	static void check(short x) {
		System.out.println("short method " + x);
	}

	static void check(long x) {
		System.out.println("long  method " + x);
	}

	static void check(double x) {
		System.out.println("double method " + x);
	}

	public static void main(String[] args) {
		char alphabet = 'z';
		SamplePromotion.check(alphabet);
	}
}
