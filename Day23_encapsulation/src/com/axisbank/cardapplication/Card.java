package com.axisbank.cardapplication;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Card {
	private long cardNo;
	private String customerName;
	private int cvv;
	private Date expiryDate;
}
