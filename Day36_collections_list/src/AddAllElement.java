import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class AddAllElement {
	public static void main(String[] args) {
		ArrayList<Integer> nums = new ArrayList<>(Arrays.asList(50, 30, 10, 20, 40));

		for (Integer i : nums) {
			System.out.println(i);
		}

		System.out.println("After sorting the elements in the collection in ascending order");
		Collections.sort(nums);

		System.out.println(nums); // [10,20,30,40,50]

		System.out.println("\n\n\nDescending order sorting");
		Collections.sort(nums, Collections.reverseOrder());

		System.out.println(nums); // [50, 40, 30, 20, 10]
	}
}
