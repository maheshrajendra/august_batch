
public class Sample {
	static void print1ToN(int n) {
		for (int i = 1; i <= n; i++) {
			System.out.print(i + " ");
		}
	}

	static void printNTo1(int n) {
		for (int i = n; i >= 1; i--) {
			System.out.print(i + " ");
		}
	}

	public static void main(String[] args) {
		print1ToN(100);
		System.out.println("\n\n\n\n__________________________________");
		printNTo1(20);
	}
}
