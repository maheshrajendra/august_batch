package com.sampleaddress;

public class Student {
	String name;
	long phoneNo;
	Address a;

	// override the tostring()
	@Override
	public String toString() {
		return "Student [name=" + name + ", phoneNo=" + phoneNo + ", a=" + a + "]";
	}

	public static void main(String[] args) {
		Student s = new Student();
		s.name = "Alpha";
		s.phoneNo = 937346546474L;
		s.a = new Address();
		s.a.no = 20;
		s.a.street = "2nd main";
		s.a.city = "Bangalore";
		s.a.pincode = 560070;
		System.out.println(s);
		System.out.println("______________________________");

		// create employee class object and assign the address object and print the
		// details
	}

}
