
public class Tablet {
	String brand;
	double price;

	void details() {
		System.out.println(brand + " " + price);
	}

	public static void main(String[] args) {
		Tablet t = new Tablet();
		t.brand = "Samsung";
		t.price = 10000;

		t.details(); // Samsung 10000

		Tablet t2 = new Tablet();
		t2.brand = "Apple";
		t2.price = 28000;

		t2.details(); // Apple 28000
		
	}
}
