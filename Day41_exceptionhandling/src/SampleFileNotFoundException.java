import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class SampleFileNotFoundException {
	public static void main(String[] args) {
		File f = new File("C:\\users\\1.txt");
		FileInputStream fs;
		try {
			fs = new FileInputStream(f);
			fs.read();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
