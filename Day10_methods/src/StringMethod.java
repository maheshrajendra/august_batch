
public class StringMethod {
	static void concat(String s1, String s2) {
		System.out.println(s1 + " " + s2);
	}

	static void concat(String s1, String s2, String s3, String s4) {
		System.out.println(s1 + " " + s2 + " " + s3 + " " + s4);
	}

	public static void main(String[] args) {
		concat("hello", "world");

		concat("Good", "Morning", "Hello", "World");
	}
}
