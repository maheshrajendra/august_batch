package carexample;

public class RunnerCar {
	public static void main(String[] args) {
		SedanCar s = new SedanCar();
		s.start();

		System.out.println("_____________________________");

		LuxuryCar car = new LuxuryCar();
		car.start();
	}
}
