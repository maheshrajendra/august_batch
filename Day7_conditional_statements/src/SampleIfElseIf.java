
public class SampleIfElseIf {
	public static void main(String[] args) {
		int marks = 60;

		if (marks >= 75) {
			System.out.println("Distinction");
		} else if (marks >= 60 && marks <= 75) {
			System.out.println("first class");
		} else if (marks >= 50 && marks <= 60) {
			System.out.println("Second class");
		} else if (marks >= 35 && marks < 50) {
			System.out.println("Pass");
		} else {
			System.out.println("Fail");
		}
	}
}
