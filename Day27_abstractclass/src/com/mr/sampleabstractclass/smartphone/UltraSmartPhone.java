package com.mr.sampleabstractclass.smartphone;

public class UltraSmartPhone extends AbstractSmartPhone {

	@Override
	void enable5g() {
		System.out.println("Enable faster 5g services");
	}

	@Override
	void call() {
		System.out.println("faster connectivity for calling");
	}

	@Override
	void sms() {
		System.out.println("Faster sms delivery...");
	}
	
	
}
