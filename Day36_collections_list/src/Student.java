import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Student implements Comparable<Student> {
	int sNo;
	String name;
	int age;
	long phoneNo;

	public Student(int sNo, String name, int age, long phoneNo) {
		this.sNo = sNo;
		this.name = name;
		this.age = age;
		this.phoneNo = phoneNo;
	}

	@Override
	public int compareTo(Student s) {
		return this.age - (s.age);
	}

	@Override
	public String toString() {
		return "Student [sNo=" + sNo + ", name=" + name + ", age=" + age + ", phoneNo=" + phoneNo + "]\n";
	}

	public static void main(String[] args) {
		Student s1 = new Student(1, "Alpha", 24, 987345683456L);
		Student s2 = new Student(3, "Beta", 22, 6474387685368L);
		Student s3 = new Student(2, "Delta", 25, 453453453455L);
		Student s4 = new Student(5, "Charlie", 23, 782746454677L);
		Student s5 = new Student(4, "Mike", 21, 8364856438568L);

		ArrayList<Student> studentList = new ArrayList<>(Arrays.asList(s1, s2, s3, s4, s5));

		System.out.println(studentList);

		Collections.sort(studentList);

		System.out.println("\n\n\nAfter sorting the student list");
		System.out.println(studentList);

		Collections.sort(studentList, Collections.reverseOrder());

		System.out.println("\n\n\nafter sorting in descending order");
		System.out.println(studentList);
	}

}
