
public class NumberMethod {

	static boolean isPositive(int n) {
		return n>=0;
	}

	public static void main(String[] args) {
		System.out.println("Result is " + isPositive(30));
	}

}
