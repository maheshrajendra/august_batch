
public class Mouse {
	String color;
	String brand;
	boolean isWireless;
	double price;
	int noOfButtons;

	void scrollUp() {
		System.out.println("Scrolling up...");
	}

	void scrollDown() {
		System.out.println("Scrolling down...");
	}

	// ALT + SHIFT + S + S -> overriding the toString method

	@Override
	public String toString() {
		return "Mouse [color=" + color + ", brand=" + brand + ", isWireless=" + isWireless + ", price=" + price
				+ ", noOfButtons=" + noOfButtons + "]";
	}

	public static void main(String[] args) {
		Mouse m1 = new Mouse();
		System.out.println("Before assigning the values");
		System.out.println(m1);
		m1.color = "Black";
		m1.brand = "Dell";
		m1.isWireless = false;
		m1.price = 275;
		m1.noOfButtons = 2;

		m1.scrollUp();

		m1.scrollDown();
		m1.scrollDown();
		m1.scrollDown();

		// instead of address of object, content of the object will be printed as I am
		// overriding the toString()
		System.out.println(m1);
	}

}
