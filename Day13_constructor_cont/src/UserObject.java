
public class UserObject {
	String emailAddress;
	String password;

	// ALT + SHIFT + S + O
	public UserObject(String emailAddress, String password) {
		this.emailAddress = emailAddress;
		this.password = password;
	}

	public UserObject(String emailAddress) {
		this(emailAddress, null);
	}

	public static void main(String[] args) {
		// this line will not work as there
		// is no zero param constructor in the class
		// UserObject u1 = new UserObject();

		UserObject u2 = new UserObject("Alpha@gmail.com", "Alpha@123");
	}
}
