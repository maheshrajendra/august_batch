
public class StringReverseUsingStringBuilderMethods {
	public static void main(String[] args) {
		String x = "Hello";
		String rev = new StringBuilder(x).reverse().toString();

		System.out.println("Actual String " + x);
		System.out.println("Reverse string " + rev);
	}
}
