import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Product implements Comparable<Product> {
	long prodId;
	String name;
	double price;

	@Override
	public int compareTo(Product p) {
		return this.name.compareTo(p.name);
	}

	@Override
	public String toString() {
		return "Product [prodId=" + prodId + ", name=" + name + ", price=" + price + "]\n";
	}

	public Product(long prodId, String name, double price) {
		this.prodId = prodId;
		this.name = name;
		this.price = price;
	}

	public static void main(String[] args) {
		Product p1 = new Product(68324686L, "Tablet", 25000.6);
		Product p2 = new Product(98346836L, "Laptop", 75000.5);
		Product p3 = new Product(18366764L, "Mobile", 4500.0);

		ArrayList<Product> productList = new ArrayList<>(Arrays.asList(p1, p2, p3));
		System.out.println(productList);

		Collections.sort(productList);
		System.out.println("\n\n\n Product list sorted based on name ascending order");
		System.out.println(productList);

		// using comparator object for sorting
		ProdIdComparator p = new ProdIdComparator();
		Collections.sort(productList, p);
		System.out.println("Sorting the object based on product id asceding order");
		System.out.println(productList);

		// using price comparator for sorting
		PriceComparator priceComparator = new PriceComparator();
		Collections.sort(productList, priceComparator);
		System.out.println("Sorting the object based on price asceding order");
		System.out.println(productList);
	}
}
