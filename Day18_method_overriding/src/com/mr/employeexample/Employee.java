package com.mr.employeexample;

public class Employee {
	String name;

	public Employee(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + "]";
	}

	public static void main(String[] args) {
		Employee e = new Employee("Alpha");
		Employee e2 = new Employee("Beta");
		System.out.println(e);

		System.out.println(e2);
	}
}
