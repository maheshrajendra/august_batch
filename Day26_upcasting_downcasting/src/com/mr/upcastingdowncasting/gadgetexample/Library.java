package com.mr.upcastingdowncasting.gadgetexample;

public class Library {

	Gadget rent(String gadgetName) {
		if ("phone".equalsIgnoreCase(gadgetName)) {
			return new Phone();
		} else if ("laptop".equalsIgnoreCase(gadgetName)) {
			return new Laptop();
		} else {
			return null;
		}
	}

	// Always think you are the main method
	public static void main(String[] args) {
		Library library = new Library();
		Gadget g = library.rent("tablet"); // Gadget g = null;
		if (g != null) {
			g.switchOn();
			if (g instanceof Phone p) {
				p.call();
			} else if (g instanceof Laptop laptop) {
				laptop.playGame();
			}
		} else {
			System.out.println("There is no such gadget in library!!!");
		}
	}

}
