import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

public class SampleMapDemostration {
	public static void main(String[] args) {
		LinkedHashMap map = new LinkedHashMap();
		map.put(1, "Alpha");
		map.put("Hello", 2);
		map.put('C', 4);
		map.put(null, 5);
		map.put(null, 6);
		map.put(7, null);
		map.put(8, null);
		map.put(true, 4.5);
		System.out.println(map); // {1=Alpha, Hello=2, C=4, null=6, 7=null, 8=null, true=4.5}
		System.out.println(map.size()); // 7
		System.out.println(map.isEmpty()); // false
		System.out.println(map.containsValue(5)); // false
		System.out.println(map.containsKey("Alpha"));// false
		System.out.println(map.containsKey(null)); // true
		System.out.println(map.get(10)); // false
		System.out.println(map.get(null)); // 6

		HashSet keys = new HashSet(map.keySet());
		System.out.println(keys);

		ArrayList values = new ArrayList(map.values());
		System.out.println(values);
	}
}
