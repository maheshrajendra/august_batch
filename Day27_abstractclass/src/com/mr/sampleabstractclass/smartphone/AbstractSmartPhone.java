package com.mr.sampleabstractclass.smartphone;

public abstract class AbstractSmartPhone {

	void call() {
		System.out.println("Calling...");
	}

	void sms() {
		System.out.println("SMS....");
	}

	abstract void enable5g();

}
