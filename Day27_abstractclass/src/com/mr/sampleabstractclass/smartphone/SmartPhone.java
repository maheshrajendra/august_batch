package com.mr.sampleabstractclass.smartphone;

public class SmartPhone extends AbstractSmartPhone {
	@Override
	void enable5g() {
		System.out.println("5g calling...");
	}

	public static void main(String[] args) {
		AbstractSmartPhone a = new SmartPhone();
		a.call();
		a.sms();
		a.enable5g();
	}
}
