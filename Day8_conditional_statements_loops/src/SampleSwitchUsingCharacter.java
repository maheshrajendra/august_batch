
public class SampleSwitchUsingCharacter {
	public static void main(String[] args) {
		char key = 'f';
		switch (key) {
		case 'm':
		case 'M':
			System.out.println("male");
			break;

		case 'f':
		case 'F':
			System.out.println("female");
			break;
			
		default:
			System.out.println("Unknown");
		}
	}
}
