import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class SampleHashMapWithGenerics {
	public static void main(String[] args) {
		HashMap<Integer, String> studentDataMap = new HashMap<>();
		studentDataMap.put(1, "Alpha");
		studentDataMap.put(2, "Beta");
		studentDataMap.put(3, "Charlie");
		studentDataMap.put(4, "Delta");

		Set<Integer> keySet = studentDataMap.keySet();
		HashSet<Integer> keys = new HashSet<>(keySet);
		System.out.println(keys); // [1,2,3,4]

		System.out.println("\n\nusing foreach and printing one by one");
		for (Integer key : keys) {
			System.out.println("Key is " + key);
		}

		Collection<String> valuesCollection = studentDataMap.values();
		ArrayList<String> values = new ArrayList<>(valuesCollection);
		System.out.println(values); // ["Alpha","Beta","Charlie","Delta"]

		System.out.println("\n\nUsing foreach and printing values one by one");
		for (String value : values) {
			System.out.println("Value is " + value);
		}

		System.out.println(studentDataMap); // {1=Alpha,2=Beta,3=Charlie,4=Delta}

		System.out.println("Using lambda expression and printing both key and values");
		studentDataMap.forEach((k, v) -> {
			System.out.println(k + "->" + v);
		});

	}
}
