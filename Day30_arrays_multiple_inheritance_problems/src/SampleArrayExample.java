import java.util.Arrays;

public class SampleArrayExample {
	public static void main(String[] args) {
		int[] nums = { 10, 20, 30, 40, 50 };

		System.out.println(Arrays.toString(nums));

		System.out.println("Printing using for each loop");
		for (int i : nums) {
			System.out.println(i + 100);
		}

		System.out.println("Priting the numbers using for loop");
		for (int i = 0; i < nums.length; i++) {
			System.out.println(nums[i]);
		}

		System.out.println("Printing the numbers in reverse");
		for (int i = nums.length - 1; i >= 0; i--) {
			System.out.println(nums[i]);
		}
	}
}
