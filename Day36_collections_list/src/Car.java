import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Car implements Comparable<Car> {
	String color;
	String regNo;
	double price;

	public Car(String color, String regNo, double price) {
		this.color = color;
		this.regNo = regNo;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Car [color=" + color + ", regNo=" + regNo + ", price=" + price + "]\n";
	}

	public static void main(String[] args) {
		Car c1 = new Car("Red", "KA05JF3421", 560000);
		Car c2 = new Car("Blue", "KA03KF8761", 684000);
		Car c3 = new Car("Green", "KA01EF9831", 458000);

		// example for adding custom non primitive objects into the ArrayList collection
		ArrayList<Car> carsList = new ArrayList<>(Arrays.asList(c1, c2, c3));
		System.out.println(carsList);

		Collections.sort(carsList);
		System.out.println("\n\n\n sorting cars in ascending price order");
		System.out.println(carsList);
	}

	@Override
	public int compareTo(Car o) {
		return (int) (this.price - o.price);
	}
}
