import java.util.Comparator;

public class ProdIdComparator implements Comparator<Product> {

	@Override
	public int compare(Product o1, Product o2) {
		return (int) (o1.prodId - o2.prodId);
	}

}
