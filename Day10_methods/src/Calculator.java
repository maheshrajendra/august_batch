
public class Calculator {

	// non-static method/instance method
	// to call/invoke this method
	// we must create object and call with
	// object reference and dot operator
	int findSquare(int num) {
		int res = num * num;
		return res;
	}

	void multiply(int a, int b) {
		System.out.println("The product of " + a + " and " + b + " is " + (a * b));
	}

	int min(int a, int b) {
		if (a < b) {
			return a;
		} else {
			return b;
		}
	}

	public static void main(String[] args) {
		Calculator c = new Calculator();
		int y = c.findSquare(4);
		System.out.println(y);

		int z = c.findSquare(25);
		System.out.println(z);

		System.out.println(y + z);

		// when a method is having return type as void
		// we cannot expect a return value
		c.multiply(4, 8); // 32

		c.multiply(10, 20); // 200

		c.multiply(2, 4); // 8

		int minimum = c.min(100, 2500);
		System.out.println("The minium of two numbers is " + minimum);
	}

}
