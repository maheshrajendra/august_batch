import java.util.Scanner;

public class StarPattern {
	public static void main(String[] args) {
		System.out.println("Enter the no. of lines");
		int n = new Scanner(System.in).nextInt();
		for (int i = 1; i <= n; i++) {

			for (int j = 1; j <= n - i; j++) {
				System.out.print(" ");
			}

			for (int k = 0; k < i; k++) {
				System.out.print("*");
			}

			System.out.println();
		}
	}
}
