package com.mr.nullpointerexplained;

public class Printer {
	static void print(String name) {
		if (name != null) {
			System.out.println(name.toUpperCase());
		}
	}

	public static void main(String[] args) {
		Printer.print("mike");

		Printer.print(null);
	}
}
