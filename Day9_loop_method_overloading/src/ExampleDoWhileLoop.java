import java.util.Scanner;

public class ExampleDoWhileLoop {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String input = null;
		do {
			System.out.println("Enter two numbers");
			int x = s.nextInt();
			int y = s.nextInt();
			System.out.println(x + y);
			System.out.println("Do you want to add some more numbers");
			input = s.next();
		} while ("yes".equalsIgnoreCase(input));
	}
}
