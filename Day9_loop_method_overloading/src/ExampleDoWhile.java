import java.util.Scanner;

public class ExampleDoWhile {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String input = null;
		do {
			System.out.println("*");
			System.out.println("Do you want to print star again?");
			input = s.next();
		} while ("yes".equalsIgnoreCase(input));
		System.out.println("End of the program");
	}
}
