import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

public class Order {
	int orderId;
	int noOfItems;
	double totalCost;

	public Order(int orderId, int noOfItems, double totalCost) {
		this.orderId = orderId;
		this.noOfItems = noOfItems;
		this.totalCost = totalCost;
	}

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", noOfItems=" + noOfItems + ", totalCost=" + totalCost + "]\n";
	}

	@Override
	public int hashCode() {
		return Objects.hash(orderId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		return orderId == other.orderId;
	}

	public static void main(String[] args) {
		Order o1 = new Order(123, 10, 567);
		Order o2 = new Order(456, 8, 87655);
		Order o3 = new Order(123, 3, 56798);
		Order o4 = new Order(567, 1, 12567);
		Order o5 = new Order(865, 5, 16567);
		Order o6 = new Order(456, 7, 800);

		HashSet<Order> ordersSet = new HashSet<>(Arrays.asList(o1, o2, o3, o4, o5, o6));
		System.out.println(ordersSet);
	}
}
