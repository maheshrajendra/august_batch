
public class Fruit {
	String color;
	boolean hasSeeds;

	Fruit(String color) {
		this.color = color;
	}

	Fruit(String color, boolean hasSeeds) {
		super();
		this.color = color;
		this.hasSeeds = hasSeeds;
	}

}
