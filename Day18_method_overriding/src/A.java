
public class A {
	A() {
		System.out.println("A");
	}

	public static void main(String[] args) {
		B b = new B();
	}
}

class B extends A {
	B() {
		System.out.println("B");
	}
}
