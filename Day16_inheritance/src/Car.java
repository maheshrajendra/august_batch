
public class Car extends Vehicle {
	boolean isAutomatic;

	void cruizeControl() {
		System.out.println("Car is on auto pilot...");
	}

	@Override
	public String toString() {
		return "Car [color=" + color + ", price=" + price + ", brand=" + brand + ", isAutomatic=" + isAutomatic + "]";
	}

}
