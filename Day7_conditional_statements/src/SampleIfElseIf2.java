
public class SampleIfElseIf2 {
	public static void main(String[] args) {
		char input = 'M';
		if (input == 'm' || input == 'M') {
			System.out.println("Male");
		} else if (input == 'f' || input == 'F') {
			System.out.println("Female");
		} else {
			System.out.println("unknown");
		}
	}
}
