
public class Duck {
	String colorOfDuck;
	int age;
	int noOfEyes = 2;
	int noOfLegs =2;
	boolean isHungry;
	
	/*
	 * this is a main method for executing duck program
	 */
	public static void main(String[] args) {
		// creating duck object
		Duck d1 = new Duck();
		Duck d2 = new Duck();
		Duck d3 = new Duck();
		Duck d4 = new Duck();
		Duck d5 = new Duck();
		System.out.println(d1.colorOfDuck);
		System.out.println("Duck program");
		
		System.out.println(d1);
	}
}
// indentation -> arranging the code properly
