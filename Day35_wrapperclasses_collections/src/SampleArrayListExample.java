import java.util.ArrayList;

public class SampleArrayListExample {
	public static void main(String[] args) {
		ArrayList list = new ArrayList();
		list.add(10);
		list.add(20.3);
		list.add(true);
		list.add("hello");
		list.add('c');
		list.add(635983797859L);

		System.out.println(list); // [10, 20.3, true, hello, c, 635983797859]
		System.out.println(list.size()); // 6
		System.out.println(list.isEmpty()); // false
		System.out.println(list.contains(20.3)); // true

		list.remove("hello");

		System.out.println(list.size()); // 5
		System.out.println(list.contains("hello")); // false

		list.clear();

		System.out.println(list.size()); // 0
	}
}
